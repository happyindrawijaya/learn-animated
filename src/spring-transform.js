/**
 * Created by hiwijaya on 3/17/17.
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
    Animated,
} from 'react-native';

//spring-transform (XY) memiliki efek untuk mengubah posisi sumbu X dan Y component + efek bounce.
export default class SpringTransform extends Component {


    constructor(props){
        super(props);

        // init animated object
        // value 0 untuk range awal, perhatikan nilai range interpolate dan default style component.
        this.animatedObject = new Animated.Value(0);

    }


    big() {
        this.animatedObject.setValue(0);    //dari range 0..
        Animated.spring(
            this.animatedObject,
            {
                toValue: 1,     //ke range 1..
                friction: 3,     //efek 'bounce'. semakin kecil semakin banyak bounce'nya. set friction = ukuran untuk hilangkan efek bounce.
            }
        ).start();
    }

    small(){
        this.setState({
            //bisa set state di sini.
        }, () => {
            this.animatedObject.setValue(1);
            Animated.spring(
                this.animatedObject,
                {
                    toValue: 0,
                    friction: 3
                }
            ).start();
        });
    }

    render(){
        const interpolateObject = this.animatedObject.interpolate({
            inputRange: [0, 1],     //Value pada object animated mengacu pada input range.
            outputRange: [0, -300],    //output dari inputrange yang di set pada value. (-): bottom -> up. (+): up -> bottom
        });

        return(
            <View style={styles.container}>
                <View style={styles.content}>




                </View>

                <View style={styles.control}>
                    <TouchableOpacity style={styles.button} onPress={() => this.big()}>
                        <Text>Show</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.small()}>
                        <Text>Hide</Text>
                    </TouchableOpacity>
                </View>
                <Animated.View style={[styles.menu, {
                        transform: [{translateX: interpolateObject}]     //set transform pada component
                    }]}>
                    <Text>INI MENU</Text>
                </Animated.View>

            </View>
        );
    };

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    button: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    content: {
        flex: 1,
        backgroundColor: 'grey',
    },
    control: {
        height: 100,
        backgroundColor: 'blue',
        zIndex: 2
    },
    menu: {
        position: 'absolute',
        right: -300,
        height: 300, //ukuran
        width: 380,
        backgroundColor: 'orange',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: 1
    }
});