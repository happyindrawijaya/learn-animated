import React, {Component} from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';
import {Actions} from 'react-native-router-flux'

export default class Home extends Component {

    toSpringscale() {
        Actions.springscale();
    }
    toSpringtransform() {
        Actions.springtransform();
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.button} onPress={() => this.toSpringscale()}>
                    <Text>Spring-Scale</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={() => this.toSpringtransform()}>
                    <Text>Spring-TranformXY</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    button: {
        width: 200,
        height: 45,
        backgroundColor: 'lightblue',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5
    }
});