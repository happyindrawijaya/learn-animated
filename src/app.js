/**
 * Created by hiwijaya on 3/17/17.
 */
import React, {Component} from 'react';
import {
    View
} from 'react-native';
import {
    Scene,
    Router
} from 'react-native-router-flux';
import Home from './home';
import SpringScale from './spring-scale';
import SpringTransform from './spring-transform';

export default class App extends Component {

    render() {
        return(
            <Router>
                <Scene key="root">
                    <Scene key="home" component={Home} title="Home" hideNavBar={true}/>
                    <Scene key="springscale" component={SpringScale} title="Spring Scale" hideNavBar={true}/>
                    <Scene key="springtransform" component={SpringTransform} title="Spring Transform" hideNavBar={true}/>
                </Scene>
            </Router>
        );
    }

}