/**
 * Created by hiwijaya on 3/17/17.
 */
import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    TouchableOpacity,
    Text,
    Animated,
} from 'react-native';

//spring-scale memiliki efek untuk mengubah scale component + efek bounce.
export default class SpringScale extends Component {


    constructor(props){
        super(props);

        // init animated object
        // value 0 untuk range awal, perhatikan nilai range interpolate dan default style component.
        this.animatedObject = new Animated.Value(0);

    }


    big() {
        this.animatedObject.setValue(0);    //dari range 0..
        Animated.spring(
            this.animatedObject,
            {
                toValue: 1,     //ke range 1..
                friction: 1,     //efek 'bounce'. semakin kecil semakin kencang bounce'nya.
                velocity: 10,    //ubah keempat property ini untuk menciptakan efek bounce yang anda inginkan.
                tension: -10
            }
        ).start();
    }

    small(){
        this.setState({
            //bisa set state di sini.
        }, () => {
            this.animatedObject.setValue(1);
            Animated.spring(
                this.animatedObject,
                {
                    toValue: 0,
                    friction: 2
                }
            ).start();
        });
    }

    render(){
        const interpolateObject = this.animatedObject.interpolate({
            inputRange: [0, 1],     //Value pada object animated mengacu pada input range.
            outputRange: [1, 2],    //output dari inputrange yang di set pada value. 1 = (ukuran x 1), 2 = (ukuran x2)
        });

        return(
            <View style={styles.container}>
                <View style={styles.content}>

                    <Animated.View style={[styles.box, {
                        transform: [{scale: interpolateObject}]     //set transform pada component
                    }]}>

                    </Animated.View>


                </View>
                <View style={styles.control}>
                    <TouchableOpacity style={styles.button} onPress={() => this.big()}>
                        <Text>Big</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => this.small()}>
                        <Text>Small</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    button: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    content: {
        flex: 9,
        backgroundColor: 'grey',
        justifyContent: 'center',
        alignItems: 'center'
    },
    control: {
        flex: 1,
        backgroundColor: 'blue'
    },
    box: {
        width: 50,  //ukuran
        height: 50, //ukuran
        backgroundColor: 'orange'
    }
});